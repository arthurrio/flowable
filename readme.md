#  Blue Pixel - Estudo Flowable

## Pre requisitos
- [Java JDK](http://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html)
- [Maven](http://mirror.nbtelecom.com.br/apache/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.zip)
- [Git](http://code.google.com/p/msysgit/downloads/detail?name=Git-1.8.1.2-preview20130201.exe)
- [curl](https://curl.haxx.se/download.html)

## Configurando ambiente

Passo a passo linha de comando:
 1. git clone https://gitlab.com/bpixel-academy/flowable.git
 2. cd flowable 
 3. mvn package
 4. java -jar target\FlowableDemo-0.0.1-SNAPSHOT.jar
 5. (Abrir outro console)
 6. curl http://localhost:8080/tasks?assignee=kermit
 Reposta esperada: [{"id":"10004","name":"my task"}]

Após testar, importe o projeto Maven no eclipse

## Leitura 



**BPMN**
https://pt.wikipedia.org/wiki/Business_Process_Model_and_Notation

**Flowable**
https://www.flowable.org/docs/userguide/index.html

Exemplo: [one-task-process.bpmn20.xml](src/main/resources/processes/one-task-process.bpmn20.xml)


## Resumo


**Conhecimentos requeridos:** Java, Eclipse, Spring, REST e Maven


**Conhecimentos adquiridos:** BPMN, Flowable, DB2 e Hadoop 


Para o workshop ou para preparação, podem vir as dúvidas sobre os conhecimentos requeridos prévios aí também, o objetivo é justamente esse ajudar a chegar ao necessário pra poder mexer no projeto com flowable.


**Desafios**

- Alterar o arquivo XML que descreve o processo de um passo, que está na pasta resources, alterando para um processo que você criou. Não invente nada muito complicado.
- Alterar o banco de dados para DB2.


- Criar endpoints na API REST para manipular o processo que você criou.


- Conectar no Hadoop também, confirmar que a aplicação conecta no Hadoop com sucesso.


- Incluir no seu processo alguma etapa que depende de envio de arquivo (imagem ou documento por exemplo).


- Fazer endpoint para receber o arquivo e armazenar no Hadoop.


- Fazer endpoint onde outro ator do processo recebe o documento.